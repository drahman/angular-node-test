// server.js
var express = require('express');
var app = express();
var path = require('path');
var http = require('http');
var bodyParser  = require('body-parser');
var mongoose = require('mongoose');
var connectionString='mongodb://localhost:27017/skyusers';
var db = mongoose.connection;
var User = require('./app/models/user');
var Access = require("./app/models/access");

mongoose.connect(connectionString);
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log("Connected to mongodb: " + connectionString);
});

app.use(express.static(__dirname + "/views"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


// API ROUTES
app.get('/', function(req, res) {
    res.json({ message: 'Running server'});
})
app.get('/users', findAllUsers);
app.get('/user/:username', findUserById);
app.post('/user', addUser);
app.put('/user/:username', updateUser);
app.delete('/user/:username', deleteUser);


// API FUNCTIONS
function findAllUsers(req, res) {

    User.find(function(err, users) {
        if(err) {
            res.json({'ERROR': err});
        } else {
            res.json(users);
        }
    });
}


function findUserById(req, res) {
    User.findOne({username:req.params.username}, function(err, user) {
        if(err) {
            res.json({'ERROR': err});
        } else {
            res.json(user);
        }
    });
}


function addUser(req, res) {
    console.log(req.body);
    var newUser = new User({
        username: req.body.username,
        password: req.body.password
    });
    newUser.save(function(err) {
        if(err) {
            res.json({'ERROR': err});
        } else {
            res.json({'SUCCESS': newUser});
        }
    });
}


function updateUser(req, res) {
    User.findOne({username:req.params.username}, function(err, user) {
        user.username = req.body.username;
        user.password = req.body.password;
        user.save(function(err) {
            if(err) {
                res.json({'ERROR': err});
            } else {
                res.json({'UPDATED': user});
            }
        });
    });
}


function deleteUser(req, res) {
    User.findOne({username:req.params.username}, function(err, user) {
        if(err) {
            res.json({'ERROR': err});
        } else {
            user.remove(function(err){
                if(err) {
                    res.json({'ERROR': err});
                } else {
                    res.json({'REMOVED': user});
                }
            });
        }
    });
}


app.listen(3000,function(){
    console.log("Server running on port 3000");
});

module.exports = app;