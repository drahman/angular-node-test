/**
 * Created by dawud on 09/12/2015.
 */
// access.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    ip_address: String,
    date: { type: Date, default: Date.now },
    action: String,
    username: String
});

module.exports = mongoose.model('access', userSchema);