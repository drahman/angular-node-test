/**
 * Created by dawud on 09/12/2015.
 */
// user.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: String,
    password: String
});


module.exports = mongoose.model('users', userSchema);