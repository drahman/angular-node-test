/**
 * Created by dawud on 09/12/2015.
 */
// test-server.js
var chai = require('chai');
var chaiHttp = require('chai-http');
var mongoose = require("mongoose");
var server = require('../server');
var User = require("../app/models/user");
var Access = require("../app/models/access");
var should = chai.should();

chai.use(chaiHttp);


describe('Check Login API', function() {

    User.collection.drop();

    beforeEach(function(done){
        var newUser = new User({
            username: 'admin',
            password: 'password'
        });
        newUser.save(function(err) {
            done();
        });
    });

    afterEach(function(done){
        //User.collection.drop();
        done();
    });



    it('should list all users on /users GET', function(done) {
        chai.request(server)
            .get('/users')
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
                res.body[0].should.have.property('username');
                res.body[0].should.have.property('password');
                res.body[0].username.should.equal('admin');
                res.body[0].password.should.equal('password');
                done();
            });
    });

    it('should list a single user on /user/<username> GET', function(done) {
        var newUser = new User({
            username: 'manager',
            password: 'password'
        });
        newUser.save(function(err, data) {
            chai.request(server)
                .get('/user/'+data.username)
                .end(function(err, res){
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('username');
                    res.body.should.have.property('password');
                    res.body.username.should.equal('manager');
                    res.body.password.should.equal('password');
                    res.body.username.should.equal(data.username);
                    done();
                });
        });
    });

    it('should add a single user on /user POST', function(done) {
        chai.request(server)
            .post('/user')
            .send({'username': 'user', 'password': 'password'})
            .end(function(err, res){
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('SUCCESS');
                res.body.SUCCESS.should.be.a('object');
                res.body.SUCCESS.should.have.property('username');
                res.body.SUCCESS.should.have.property('password');
                res.body.SUCCESS.username.should.equal('user');
                res.body.SUCCESS.password.should.equal('password');
                done();
            });
    });

    it('should update a single user on /user/<username> PUT', function(done) {
        chai.request(server)
            .get('/user')
            .end(function(err, res){
                chai.request(server)
                    .put('/user/'+res.body[0].username)
                    .send({'username': 'admin'})
                    .end(function(error, response){
                        response.should.have.status(200);
                        response.should.be.json;
                        response.body.should.be.a('object');
                        response.body.should.have.property('UPDATED');
                        response.body.UPDATED.should.be.a('object');
                        response.body.UPDATED.should.have.property('username');
                        response.body.UPDATED.should.have.property('_id');
                        response.body.UPDATED.username.should.equal('password');
                        done();
                    });
            });
    });

    it('should delete a single user on /user/<username> DELETE', function(done) {
        chai.request(server)
            .get('/user')
            .end(function(err, res){
                chai.request(server)
                    .delete('/user/'+res.body[0].username)
                    .end(function(error, response){
                        response.should.have.status(200);
                        response.should.be.json;
                        response.body.should.be.a('object');
                        response.body.should.have.property('REMOVED');
                        response.body.REMOVED.should.be.a('object');
                        response.body.REMOVED.should.have.property('username');
                        response.body.REMOVED.username.should.equal(username);
                        done();
                    });
            });
    });

});