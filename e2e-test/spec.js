/**
 * Created by dawud on 09/12/2015.
 */
//spec.js
describe('Angular node test', function() {

    beforeEach(function() {
        browser.get('http://localhost:3000');
    });

    it('should have a title', function() {
        expect(browser.getTitle()).toEqual('Angular node test');
    });


    it('login', loginUser());
    it('uri that requires user to be logged in', function(done){
        server
            .get('/login')
            .expect(200)
            .end(function(err, res){
                if (err) return done(err);
                console.log(res.body);
                done()
            });
    });
});


function loginUser() {
    return function(done) {
        server
            .post('/login')
            .send({ username: 'admin', password: 'password' })
            .expect(302)
            .expect('Location', '/loggedin')
            .end(onResponse);

        function onResponse(err, res) {
            if (err) return done(err);
            return done();
        }
    };
};