/**
 * Created by dawud on 09/12/2015.
 */
// conf.js
exports.config = {
    directConnect: true,

    framework: 'jasmine2',

    specs: [
        'spec.js'
    ],

    capabilities: {
        'browserName': 'chrome'
    },
};
