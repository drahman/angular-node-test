/**
 * Created by dawud on 09/12/2015.
 */
// controller.js
var app = angular.module('loginApp', []);
app.controller('AppCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.login = function() {
        console.log($scope.user);
        $http.post('/login', $scope.user).success( function(response) {
            console.log(response);
        });
    };


}]);